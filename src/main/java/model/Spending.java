package model;

public class Spending {
    public enum Category { GROCERY, CAR };
    private int amount;
    private String description = "";
    private Category category;

    public Spending(int amount, Category category) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount should be above zero");
        }
        this.amount = amount;
        this.category = category;
    }

    public Spending(int amount, String description, Category category) {
        this(amount, category);
        this.description = description;
        this.category = category;
    }

    public int getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public Category getCategory() {
        return category;
    }

    public String toString() {
        return getCategory() + " " + getAmount()  + " " + getDescription();
    }
}
