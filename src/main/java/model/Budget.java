package model;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class Budget {
    private Map<Spending.Category, Integer> amountPerCategory;
    public Budget() {
        amountPerCategory = new HashMap<>();
        initializeMap(amountPerCategory);
    }

    private void initializeMap(Map<Spending.Category, Integer> map) {
        for (Spending.Category category : Spending.Category.values()) {
            map.put(category, 0);
        }
    }

    public int getBudgetOfCategory(Spending.Category category) {
        return amountPerCategory.get(category);
    }

    public void setBudget(Spending.Category category, int amount) {
        amountPerCategory.put(category, amount);
    }

    public Map<Spending.Category, Integer> compare(Month month) {
        List<Month> months = new ArrayList<>();
        months.add(month);
        return compare(months);
    }

    public Map<Spending.Category, Integer> compare(List<Month> months) {
        Map<Spending.Category, Integer> compareResult = new HashMap<>();
        initializeMap(compareResult);
        for (Spending.Category category : amountPerCategory.keySet()) {
            int categoryResult = amountPerCategory.get(category) * months.size();
            for (Month month : months) {
                categoryResult -= month.sumOfSpendings(category);
            }
            compareResult.put(category, categoryResult);
        }
        return compareResult;
    }
}
