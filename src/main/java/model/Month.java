package model;

import java.util.Scanner;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
public class Month {
    public enum Name { JANUARY, FEBRUARY, MARS, APRIL, MAY,
                       JUNE, JULY, AUGUST, SEPTEMBER,
                       OCTOBER, NOVEMBER, DECEMBER };
    private List<Spending> spendings = new ArrayList<>();
    private int year;
    private Name name;

    public Month(int year, Name name) {
        this.year = year;
        this.name = name;
    }

    public int getYear() {
        return year;
    }
    
    public Name getName() {
        return name;
    }

    public void addSpending(Spending spending) {
        if (spending == null) {
            throw new IllegalArgumentException("Spending should not be null");
        }
        spendings.add(spending);
    }

    public Spending getSpending(int index) {
        return spendings.get(index);
    }

    public int sumOfSpendings() {
        return new SumOfAllSpendings().sum(spendings);
    }

    public int sumOfSpendings(Spending.Category category) {
        return new SumOfSpendingsInCategory(category).sum(spendings);
    }

    public int size() {
        return spendings.size();
    }

    public static Month load(int year, Name name) {
        Month month = new Month(year, name);
        try {
            Scanner scanner = new Scanner(month.getSaveFile());
            while (scanner.hasNext()) {
                month.addSpending(fromStringToSpending(scanner.nextLine()));
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return month;
    }

    private static Spending fromStringToSpending(String s) {
        String[] words = s.split(" ");
        Spending.Category category = Spending.Category.valueOf(words[0]);
        int amount = Integer.parseInt(words[1]);
        String description = "";
        if (words.length > 2) {
            description = s.substring(words[0].length() + words[1].length() + 2);
        }
        return new Spending(amount, description, category);
    }

    public void save() {
        try {
            createSaveDirectory();
            File file = getSaveFile();
            PrintWriter writer = new PrintWriter(file);
            for (Spending spending : spendings) {
                writer.println(spending);
            }
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private File getSaveFile() {
        return new File("save/" + String.valueOf(getName() + " " + getYear()) + ".txt"); 
    }

    private void createSaveDirectory() throws IOException {
        new File("save").mkdir();
    }

    private abstract static class SumOfSpendings {
        protected int sum(List<Spending> spendings) {
            int sum = 0;
            for (Spending spending : spendings) {
                if (spendingIsIncluded(spending)) {
                    sum += spending.getAmount();
                }
            }
            return sum;
        }

        protected abstract boolean spendingIsIncluded(Spending spending);
    }

    private static class SumOfAllSpendings extends SumOfSpendings {
        protected boolean spendingIsIncluded(Spending spending) {
            return true;
        }
    }

    private static class SumOfSpendingsInCategory extends SumOfSpendings {
        private Spending.Category category;

        private SumOfSpendingsInCategory(Spending.Category category) {
            this.category = category;
        }
        protected boolean spendingIsIncluded(Spending spending) {
            return spending.getCategory().equals(category);
        }
    }
}
