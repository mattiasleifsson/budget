package controller;

import model.*;
import java.lang.NumberFormatException;
import java.util.Scanner;
import java.io.InputStream;
public class Input {

    private InputStream in;
    private Scanner scanner;
    public Input(InputStream in) {
        this.in = in;
        this.scanner = new Scanner(in);
    }   
    public int readInt(int min, int max) {
        String userInput = "";
        while (true) {
            userInput = scanner.nextLine();
            if (tryParse(userInput) && isWithinBounds(min, max, Integer.parseInt(userInput))) {
                return Integer.parseInt(userInput);
            }
        }
    }

    private boolean isWithinBounds(int min, int max, int value) {
        return value >= min && value < max;
    }

    private boolean tryParse(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public <E extends Enum<E>> E readEnum(Class<E> enumClass) {
        return enumClass.getEnumConstants()[readInt(0, enumClass.getEnumConstants().length)];
    }

    public String readString() {
        return scanner.nextLine();
    }
}
