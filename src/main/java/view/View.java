package view;

import java.util.Scanner;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Calendar;
import controller.*;
import model.*;
public class View {

    private Input input;
    private OutputStream out;
    public View(Input input, OutputStream out) {
        this.input = input;
        this.out = out;
    }

    private void startMenu() {
        try {
            while (true) {
                out.write("0. Add spending\n".getBytes());
                out.write("1. View month\n".getBytes());
                int userChoice = input.readInt(0, 2);
                if (userChoice == 0) {
                    addSpending();
                } else {
                    printMonth();
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void addSpending() throws IOException {
        out.write("Year:\n".getBytes());
        int year = input.readInt(0, 99999);
        out.write("Select month:\n".getBytes());
        printEnum(Month.Name.class);
        Month.Name name = input.readEnum(Month.Name.class);
        out.write("Select category:\n".getBytes());
        printEnum(Spending.Category.class);
        Spending.Category category = input.readEnum(Spending.Category.class);
        out.write("Amount:\n".getBytes());
        int amount = input.readInt(1, 99999999);
        out.write("Description:\n".getBytes());
        String description = input.readString();
        Month month = Month.load(year, name);
        Spending spending = new Spending(amount, description, category);
        month.addSpending(spending);
        month.save();
    }

    private <E extends Enum<E>> void printEnum(Class<E> enumClass) throws IOException {
        for (E e : enumClass.getEnumConstants()) {
            out.write((e.toString() + "\n").getBytes());
        }
    }

    public void printMonth() {
        int year = input.readInt(0, 9999);
        Month.Name name = input.readEnum(Month.Name.class);
        Month month = Month.load(year, name);
        try {
            for (int i = 0; i < month.size(); i++) {
                out.write(month.getSpending(i).toString().getBytes());
                out.write('\n');
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        Input input = new Input(System.in);
        View view = new View(input, System.out);
    }
}
