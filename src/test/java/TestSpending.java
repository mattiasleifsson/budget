import model.*;
import org.junit.*;
import static org.junit.Assert.*;
public class TestSpending {
    Spending spending;

    @Test
    public void spendingShouldReturnCorrectAmount() {
        spending = new Spending(10, Spending.Category.CAR);
        assertEquals(10, spending.getAmount());
    }


    @Test(expected = IllegalArgumentException.class)
    public void spendingAmountShouldBeAboveZero() {
        spending = new Spending(-34, Spending.Category.CAR);
    }

    @Test
    public void spendingShouldBeAbleToHaveDescription() {
        spending = new Spending(4, "Food", Spending.Category.CAR);
    }

    @Test
    public void spendingShouldRememberDescription() {
        spending = new Spending(43, "Gas", Spending.Category.CAR);
        assertEquals("Gas", spending.getDescription());
    }

    @Test
    public void spendingWithUnspecifiedDescriptionShouldHaveEmptyDescription() {
        spending = new Spending(34, Spending.Category.CAR);
        assertEquals("", spending.getDescription());
    }

    @Test
    public void spendingShouldHaveCategory() {
        spending = new Spending(34, Spending.Category.GROCERY);
        spending = new Spending(324, "Gas", Spending.Category.CAR);
    }

    @Test
    public void spendingShouldRememberCorrectCategory() {
        spending = new Spending(5634, Spending.Category.GROCERY);
        assertEquals(Spending.Category.GROCERY, spending.getCategory());
    }

    @Test
    public void spendingToStringShouldBeCorrectFormat() {
        spending = new Spending(342423, "Breakfast", Spending.Category.GROCERY);
        assertEquals(Spending.Category.GROCERY.toString() + " 342423 Breakfast", spending.toString());
    }
}
