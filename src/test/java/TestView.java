import view.*;
import model.*;
import controller.*;
import java.io.IOException;
import java.util.Scanner;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import org.junit.*;
import static org.junit.Assert.*;
public class TestView {
    private View view;
    private Input input;
    private InputStream in;
    private OutputStream out;
    private int nbrOfMonthNames = Month.Name.values().length;
    private int nbrOfSpendingCategories = Spending.Category.values().length;

    @After
    public void tearDown() {
        FileCleaner.deleteAllSavedMonths();
    }
    @Test
    public void viewShouldRememberSpendingAddedWithAddSpending() throws IOException {
        in = new ByteArrayInputStream("2017\n0\n1\n32\nGas money\n2017\n0".getBytes());
        input = new Input(in);
        out = new ByteArrayOutputStream();
        view = new View(input, out);
        view.addSpending();
        view.printMonth();
        assertEquals("CAR 32 Gas money\n", skipLines(5 + nbrOfMonthNames + nbrOfSpendingCategories, out));
    }
    @Test
    public void printMonthShouldPrintInCorrectOrder() throws IOException {
        in = new ByteArrayInputStream("2017\n0\n1\n32\nGas money\n2017\n0\n0\n100\n\n2017\n0".getBytes());
        input = new Input(in);
        out = new ByteArrayOutputStream();
        view = new View(input, out);
        view.addSpending();
        view.addSpending();
        view.printMonth();
        assertEquals("CAR 32 Gas money\nGROCERY 100 \n", skipLines(2 * (5 + nbrOfMonthNames + nbrOfSpendingCategories), out));
    }

    private String skipLines(int nbrOfLinesToSkip, OutputStream out) {
        String[] lines = out.toString().split("\n");
        StringBuilder sb = new StringBuilder();
        for (int i = nbrOfLinesToSkip; i < lines.length; i++) {
            sb.append(lines[i] + "\n");
        }
        return sb.toString();
    }
}
