import java.io.File;
public class FileCleaner {
    public static void deleteAllSavedMonths() {
        File saveDirectory = new File("save");
        if (saveDirectory.exists()) {
            for (File file : saveDirectory.listFiles()) {
                file.delete();
            }
        }
        saveDirectory.delete();
    }
}
