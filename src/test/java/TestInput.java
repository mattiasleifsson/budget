import model.*;
import controller.*;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.junit.*;
import static org.junit.Assert.*;
public class TestInput {
    private Input input;
    private InputStream in;

    @Test
    public void readIntShouldReturnCorrectInt() {
        in = new ByteArrayInputStream("3".getBytes());
        input = new Input(in);
        assertEquals(3, input.readInt(0, 5));
    }

    @Test
    public void readIntShouldReturnInCorrectOrder() {
        in = new ByteArrayInputStream("3\n-65\n123".getBytes());
        input = new Input(in);
        assertEquals(3, input.readInt(-100, 500));
        assertEquals(-65, input.readInt(-100, 500));
        assertEquals(123, input.readInt(-100, 500));
    }


    @Test
    public void readIntShouldWaitForCorrectInputAfterWrongInput() {
        in = new ByteArrayInputStream("NotAnInteger\n11\n3".getBytes());
        input = new Input(in);
        assertEquals(3, input.readInt(0, 10));
    }

    @Test
    public void readEnumShouldReturnCorrectEnum() {
        in = new ByteArrayInputStream("4".getBytes());
        input = new Input(in);
        assertEquals(Month.Name.MAY, input.readEnum(Month.Name.class));
    }

    @Test
    public void readEnumShouldAcceptDifferentEnums() {
        in = new ByteArrayInputStream("4\n1".getBytes());
        input = new Input(in);
        assertEquals(Month.Name.MAY, input.readEnum(Month.Name.class));
        assertEquals(Spending.Category.CAR, input.readEnum(Spending.Category.class));
    }

    @Test
    public void readEnumShouldCheckThatInputIsWithinBounds() {
        in = new ByteArrayInputStream("15\n1".getBytes());
        input = new Input(in);
        assertEquals(Month.Name.FEBRUARY, input.readEnum(Month.Name.class));
    }

    @Test
    public void readStringShouldReturnCorrectString() {
        in = new ByteArrayInputStream("This\nis a test".getBytes());
        input = new Input(in);
        assertEquals("This", input.readString());
        assertEquals("is a test", input.readString());
    }
}
