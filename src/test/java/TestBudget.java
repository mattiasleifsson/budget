import model.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.junit.*;
import static org.junit.Assert.*;
public class TestBudget {
    private Budget budget;
    
    @Test
    public void newBudgetShouldHaveAllCategoriesAsZero() {
        budget = new Budget();
        for (Spending.Category category : Spending.Category.values()) {
            assertEquals(0, budget.getBudgetOfCategory(category));
        }
    }

    @Test
    public void budgetShouldRememberCorrectValueOfCategories() {
        budget = new Budget();
        budget.setBudget(Spending.Category.CAR, 5);
        assertEquals(5, budget.getBudgetOfCategory(Spending.Category.CAR));
        assertEquals(0, budget.getBudgetOfCategory(Spending.Category.GROCERY));
    }

    @Test
    public void newBudgetComparingMonthWithNoSpendingsShouldReturnZeroForAllCategories() {
        budget = new Budget();
        Month month = new Month(2017, Month.Name.JANUARY);
        for (Spending.Category category : Spending.Category.values()) {
            assertEquals((int)0, (int)budget.compare(month).get(category));
        }
    }

    @Test
    public void budgetComparingMonthShouldReturnCorrectValue() {
        budget = new Budget();
        budget.setBudget(Spending.Category.GROCERY, 500);
        Month month = new Month(2001, Month.Name.DECEMBER);
        month.addSpending(new Spending(32, Spending.Category.GROCERY));
        month.addSpending(new Spending(132, Spending.Category.CAR));
        assertEquals(468, (int)budget.compare(month).get(Spending.Category.GROCERY));
    }

    @Test
    public void budgetComparingOverspentMonthShouldReturnNegativeValue() {
        budget = new Budget();
        budget.setBudget(Spending.Category.GROCERY, 100);
        Month month = new Month(2001, Month.Name.DECEMBER);
        month.addSpending(new Spending(143, Spending.Category.GROCERY));
        month.addSpending(new Spending(132, Spending.Category.CAR));
        assertEquals(-43, (int)budget.compare(month).get(Spending.Category.GROCERY));
    }

    @Test
    public void budgetShouldBeAbleToCompareSeveralMonths() {
        budget = new Budget();
        budget.setBudget(Spending.Category.GROCERY, 500);
        budget.setBudget(Spending.Category.CAR, 100);
        Month month1 = new Month(2001, Month.Name.DECEMBER);
        month1.addSpending(new Spending(143, Spending.Category.GROCERY));
        month1.addSpending(new Spending(132, Spending.Category.CAR));
        Month month2 = new Month(2002, Month.Name.JANUARY);
        month2.addSpending(new Spending(23, Spending.Category.GROCERY));
        List<Month> months = new ArrayList<>();
        months.add(month1);
        months.add(month2);
        Map<Spending.Category, Integer> compareResult = budget.compare(months);
        assertEquals(500 * 2 - 143 - 23, (int)compareResult.get(Spending.Category.GROCERY));
        assertEquals(100 * 2 - 132, (int)compareResult.get(Spending.Category.CAR));
    }
}
