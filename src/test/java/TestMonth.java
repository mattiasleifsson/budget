import model.*;
import java.io.File;
import org.junit.*;
import static org.junit.Assert.*;
public class TestMonth {
    Month month = new Month(5, Month.Name.FEBRUARY);

    @After
    public void tearDown() {
        FileCleaner.deleteAllSavedMonths();
    }

    @Test
    public void monthShouldBeAbleToContainSpending() {
        Spending spending = new Spending(10, Spending.Category.CAR);
        month.addSpending(spending);
        assertEquals(spending, month.getSpending(0));
    }

    @Test
    public void monthShouldBeAbleToContainSeveralSpendings() {
        Spending spending1 = new Spending(10, Spending.Category.CAR);
        Spending spending2 = new Spending(2332, Spending.Category.CAR);
        month.addSpending(spending1);
        month.addSpending(spending2);
        assertEquals(spending1, month.getSpending(0));
        assertEquals(spending2, month.getSpending(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthShouldNotAllowNullSpendings() {
        month.addSpending(null);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void monthWithoutSpendingShouldNotBeAbleToReturnSpending() {
        month.getSpending(0);
    }

    @Test
    public void monthShouldContainYear() {
        month = new Month(2017, Month.Name.FEBRUARY);
        assertEquals(2017, month.getYear());
    }

    @Test
    public void monthShouldHaveMonthName() {
        month = new Month(2017, Month.Name.JANUARY);
        assertEquals(Month.Name.JANUARY, month.getName());
    }
    
    @Test
    public void sumOfNoSpendingsShouldBeZero() {
        month = new Month(2017, Month.Name.JANUARY);
        assertEquals(0, month.sumOfSpendings());
    }

    @Test
    public void sumOfSpendingsShouldBeCorrect() {
        month = new Month(2017, Month.Name.JANUARY);
        month.addSpending(new Spending(32, Spending.Category.CAR));
        month.addSpending(new Spending(22, Spending.Category.CAR));
        assertEquals(54, month.sumOfSpendings());
    }

    @Test
    public void sumOfNoSpendingsInCategoryShouldBeZero() {
        month = new Month(2017, Month.Name.JANUARY);
        assertEquals(0, month.sumOfSpendings(Spending.Category.GROCERY));
    }

    @Test
    public void sumOfSpendingsInCategoryShouldBeCorrect() {
        month = new Month(2000, Month.Name.JULY);
        month.addSpending(new Spending(332, Spending.Category.CAR));
        month.addSpending(new Spending(10, Spending.Category.CAR));
        month.addSpending(new Spending(43, Spending.Category.GROCERY));
        assertEquals(342, month.sumOfSpendings(Spending.Category.CAR));
    }

    @Test
    public void loadedMonthShouldHaveCorrectYearAndMonthName() {
        month = Month.load(2017, Month.Name.JANUARY);
        assertEquals(2017, month.getYear());
        assertEquals(Month.Name.JANUARY, month.getName());
    }

    @Test
    public void loadingUnsavedMonthShouldReturnEmptyMonth() {
        month = Month.load(56, Month.Name.MARS);
        assertEquals(0, month.sumOfSpendings());
    }

    @Test
    public void savingMonthShouldCreateFile() {
        month = new Month(2017, Month.Name.FEBRUARY);
        month.save();
        File file = new File("save/" + String.valueOf(month.getName() + " " + month.getYear()) + ".txt");
        assertTrue(file.exists());
        assertTrue(file.isFile());
    }

    @Test
    public void loadedMonthShouldContainCorrectSpending() {
        month = new Month(2017, Month.Name.FEBRUARY);
        Spending spending = new Spending(765, "New tire", Spending.Category.CAR);
        month.addSpending(spending);
        month.save();
        Month loadedMonth = Month.load(2017, Month.Name.FEBRUARY);
        assertEquals(spending.toString(), loadedMonth.getSpending(0).toString());
    }

    @Test
    public void savingMonthSeveralTimesShouldOverride() {
        month = new Month(2032, Month.Name.MARS);
        Spending spending = new Spending(3424, Spending.Category.GROCERY);
        month.addSpending(spending);
        month.save();
        month.save();
        month.save();
        Month loadedMonth = Month.load(2032, Month.Name.MARS);
        assertEquals(3424, loadedMonth.sumOfSpendings());
    }

    @Test
    public void newMonthShouldSaveOverOldFile() {
        month = new Month(2032, Month.Name.MARS);
        month.addSpending(new Spending(3424, Spending.Category.GROCERY));
        month.save();

        Month monthNew = new Month(2032, Month.Name.MARS);
        monthNew.addSpending(new Spending(33, Spending.Category.GROCERY));
        monthNew.save();

        Month loadedMonth = Month.load(2032, Month.Name.MARS);
        assertEquals(33, loadedMonth.sumOfSpendings());
    }

    @Test
    public void loadedMonthShouldHaveCorrectSumOfSpendings() {
        month = new Month(342, Month.Name.JUNE);
        month.addSpending(new Spending(12, Spending.Category.GROCERY));
        month.addSpending(new Spending(30, Spending.Category.GROCERY));
        month.addSpending(new Spending(5, Spending.Category.CAR));
        month.save();
        Month loadedMonth = Month.load(342, Month.Name.JUNE);
        assertEquals(42, loadedMonth.sumOfSpendings(Spending.Category.GROCERY));
    }

    @Test
    public void loadedMonthShouldHaveCorrectSpendingDescriptions() {
        month = new Month(111, Month.Name.APRIL);
        month.addSpending(new Spending(3323, "Description 1", Spending.Category.GROCERY));
        month.addSpending(new Spending(3323, "Description 2", Spending.Category.GROCERY));
        month.save();
        Month loadedMonth = Month.load(111, Month.Name.APRIL);
        assertEquals("Description 1", loadedMonth.getSpending(0).getDescription());
        assertEquals("Description 2", loadedMonth.getSpending(1).getDescription());
    }
    
    @Test
    public void loadedMonthShouldHaveSpendingsInRightOrder() {
        month = new Month(123, Month.Name.FEBRUARY);
        month.addSpending(new Spending(1, Spending.Category.GROCERY));
        month.addSpending(new Spending(2, Spending.Category.GROCERY));
        month.addSpending(new Spending(3, Spending.Category.GROCERY));
        month.save();
        Month loadedMonth = Month.load(123, Month.Name.FEBRUARY);
        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, loadedMonth.getSpending(i).getAmount());
        }

    }

    @Test
    public void emptyMonthShouldHaveZeroSize() {
        month = new Month(1, Month.Name.MARS);
        assertEquals(0, month.size());
    }

    @Test
    public void monthShouldHaveCorrectSize() {
        month = new Month(1, Month.Name.MARS);
        month.addSpending(new Spending(32, Spending.Category.CAR));
        month.addSpending(new Spending(5, Spending.Category.CAR));
        assertEquals(2, month.size());
    }
}
